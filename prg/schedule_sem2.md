# Календарный план "Программирование" (Семестр 2)

### Занятия

| # | Date | Topic |
| --- | --- |--- |
| 1 | 18.02 | Вводная. Big O |
| 2 | 25.02 | Collections |
| 3 | 04.03 | Maven. Unit tests. Debugging. |
| 4 | 11.03 | Oblivion |
| 5 | 18.03 | Lambdas. Streams |
| 6 | 25.03 | IO. Files |
| 7 | 01.04 | Threads. Mutex. Synchronizers. |
| 8 | 08.04 | ExecutorServices. Future. CompletableFuture.|
| 9 | 15.04 | - |
| 10 | 22.04 | SQL+JDBC |
| 11 | 29.04 | - |
| 12 | 06.05 | - |
| 13 | 13.05 | JavaFX. Swing. TG Bots. |
| 14 | 20.05 |  |
| 15 | 27.05 |  | 

### Autocode Deadlines

| Блок упражнений | Мягкий Дедлайн | Дедлайн |
| --- | --- | --- |
| Meet Autocode| 11.03  | 31.03  |
| Java Fundamentals | 17.03  | 07.04 |
| Java Collections | 24.03  | 14.04 |
| JUnit | 31.03  | 21.04 |
| Java Streams | 14.04  | 28.04 |
| IO | 21.04  | 05.05 |
| Concurrency | 05.05  | 19.05 |
| Java + SQL. JDBC | 19.05  | 02.06 |

### Project Stages

| Этап | Мягкий Дедлайн | Дедлайн |
| --- | --- | --- |
| Подача заявки. High-Level Overview | 14.06  | 14.06  |
| Проектирование. Design Document. | 14.06  | 14.06 |
| Реализация. Project overview. Demo. | 17.06  | 01.07 |

- Если не успеть в **Мягкий Дедлайн**, применяется понижающий коэффициент **0,8**
- Если не успеть в **Дедлайн**, применяется понижающий коэффициент **0,6**
